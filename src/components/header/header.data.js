// item pada menu header
export default [
  {
    path: 'home',
    label: 'Home',
  },
  {
    path: 'feature',
    label: 'Features',
  },
  {
    path: 'pricing',
    label: 'Pricing',
  },
  {
    path: 'testimonial',
    label: 'Testimonial',
  },
];
